package com.test.conexa.testeConexa.controller;

import com.test.conexa.testeConexa.model.Scheduling;
import com.test.conexa.testeConexa.service.SchedulingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/conexa/agendamento")
public class SchedulingController {

    @Autowired
    public SchedulingController(SchedulingService schedulingService) {
        this.schedulingService = schedulingService;
    }

    private SchedulingService schedulingService;

    @PostMapping("/create")
    public ResponseEntity createSchedulling(@RequestBody Scheduling schedulling, @RequestHeader String Authorization){

       schedulingService.createScheduling(schedulling,Authorization);

        return (ResponseEntity) ResponseEntity.status(HttpStatus.CREATED);
    }

}
