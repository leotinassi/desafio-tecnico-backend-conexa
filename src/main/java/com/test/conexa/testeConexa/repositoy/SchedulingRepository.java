package com.test.conexa.testeConexa.repositoy;

import com.test.conexa.testeConexa.model.Scheduling;
import org.springframework.data.repository.CrudRepository;

public interface SchedulingRepository extends CrudRepository<Scheduling, Long> {

}
