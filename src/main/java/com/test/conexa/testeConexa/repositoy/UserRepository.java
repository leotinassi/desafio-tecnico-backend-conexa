package com.test.conexa.testeConexa.repositoy;

import com.test.conexa.testeConexa.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findByEmail(String email);
}
