package com.test.conexa.testeConexa.dto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class DadosLogin {

    private String email;
    private String senha;

}
