package com.test.conexa.testeConexa.dto;
import com.test.conexa.testeConexa.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class UserAutheticatedDTO{


    private String tipo;
    private String email;
    private String nome;
    private String token;

    public static UserAutheticatedDTO toDTO(User user, String tipo) {
        return new UserAutheticatedDTO(user.getEmail(), user.getNome(), user.getToken(), tipo);
    }

}
