package com.test.conexa.testeConexa.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.test.conexa.testeConexa.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonAutoDetect(fieldVisibility= JsonAutoDetect.Visibility.ANY)
public class UserResponserDTO {
    private Long id;
    private String nome;
    private String email;
    private String senha;

    public UserResponserDTO(String nome, String email, String senha) {
        this.nome = nome;
        this.email = email;
        this.senha = senha;
    }

    public static  UserResponserDTO toDTO(User user){
        return new UserResponserDTO(user.getNome(), user.getEmail(), user.getSenha());
    }



}
