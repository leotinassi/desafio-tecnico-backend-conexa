package com.test.conexa.testeConexa.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.test.conexa.testeConexa.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.Column;
import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class UserRegistrationDTO{

    private Long id;

    private String nome;
    private String email;
    private String senha;
    private String confirmacaoSenha;
    private String especialidade;
    private String cpf;
    private Timestamp dataNascimento;
    private String telefone;

    public User toUser(){
        return new User(getNome(), getEmail(), getSenha(), getConfirmacaoSenha());
    }

}
