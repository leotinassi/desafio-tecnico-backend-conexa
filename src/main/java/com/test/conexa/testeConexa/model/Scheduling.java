package com.test.conexa.testeConexa.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Scheduling {

    @Id
    private Long id;

    @Column
    private Timestamp dataAgendamento;

    @Column
    private Timestamp horaAgendamento;

    @Column
    private String paciente;

    @Column
    @CPF
    private String cpfPaciente;

    @Column
    private String nomeDr;
}
