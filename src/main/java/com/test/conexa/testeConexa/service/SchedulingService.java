package com.test.conexa.testeConexa.service;

import com.test.conexa.testeConexa.exceptions.ExpiredTokenException;
import com.test.conexa.testeConexa.exceptions.InvalidTokenException;
import com.test.conexa.testeConexa.model.Scheduling;
import com.test.conexa.testeConexa.repositoy.SchedulingRepository;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class SchedulingService{

    @Autowired
    public SchedulingService(SchedulingRepository schedulingRepository, TokenService tokenService) {
        this.schedulingRepository = schedulingRepository;
        this.tokenService = tokenService;
    }


    private SchedulingRepository schedulingRepository;
    private TokenService tokenService;

    public Scheduling createScheduling (Scheduling agend, String token){
        Scheduling agendamento = null;
        if (!token.isEmpty() && validate(token)) {
            agendamento = schedulingRepository.save(agend);
        }
        return agendamento;
    }
    private boolean validate(String token) {
        try {
            String tokenTratado = token.replace("Bearer ", "");
            Claims claims = tokenService.decodeToken(tokenTratado);

            System.out.println(claims.getIssuer());
            System.out.println(claims.getIssuedAt());
            //Verifica se o token está expirado
            if (claims.getExpiration().before(new Date(System.currentTimeMillis()))) throw new ExpiredTokenException();
            System.out.println(claims.getExpiration());
            return true;
        } catch (ExpiredTokenException et){
            et.printStackTrace();
            throw et;
        } catch (Exception e) {
            e.printStackTrace();
            throw new InvalidTokenException();
        }

    }
}
