package com.test.conexa.testeConexa.service;

import com.test.conexa.testeConexa.exceptions.ConfirmationPass;
import com.test.conexa.testeConexa.model.User;
import com.test.conexa.testeConexa.repositoy.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRegistrationService {

    private UserRepository userRepository;
    private TokenService tokenService;

    @Autowired
    public UserRegistrationService(UserRepository userRepository, TokenService tokenService) {
        this.userRepository = userRepository;
        this.tokenService = tokenService;
    }

    public User registrate(User user){
        user.setToken(tokenService.generateToken(user));
        if (!user.getSenha().equals(user.getConfirmacaoSenha()))
            throw new ConfirmationPass("Há divergencia entre as duas senhas.");
        return userRepository.save(user);
    }



}
