package com.test.conexa.testeConexa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesteConexaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesteConexaApplication.class, args);
	}

}
